#include "Button.h"
#include <Arduino.h>

Button::Button():
    _debounceTime(50),
    _lastDebounce(0),
    _buttonState(LOW),
    _lastButtonState(LOW)
{}

void Button::setPin(int pin) {
    _pin = pin;
}

int Button::getState() {
    _buttonState = digitalRead(_pin);

    if (_buttonState != _lastButtonState) {
        _lastDebounce = millis();
        _lastButtonState = _buttonState;
    }

    if (millis() - _lastDebounce > _debounceTime) {
        return _buttonState;
    }

    return LOW;
}
