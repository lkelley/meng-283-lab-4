#include <Arduino.h>
#include "Button.h"

// Function declarations
void flashTest();
void setPinModes();

// Pin CONSTs
const short RED_LED = 7;
const short GREEN_LED = 8;
const short BUTTON = 12;
const short RED_DURATION = 1000;
const short GREEN_DURATION = 2000;

// Timer vars
unsigned long startTime;
unsigned long halfPeriod;
unsigned short light;
unsigned long currentTime;
int lastButtonState;
int currentButtonState;

// Button object
Button button;


/**
 * Initial setup
 */
void setup() {
    setPinModes();
    flashTest();

    button.setPin(BUTTON);

    lastButtonState = button.getState();
}


/**
 * Main loop
 */
void loop() {
    // Capture current time
    currentTime = millis();

    // Capture current button state
    currentButtonState = button.getState();

    // Choose light based on button state
    if (currentButtonState == HIGH) { // Flash red LED
        light = RED_LED;
        halfPeriod = RED_DURATION;
        digitalWrite(GREEN_LED, LOW);
    } else { // Flash green LED
        light = GREEN_LED;
        halfPeriod = GREEN_DURATION;
        digitalWrite(RED_LED, LOW);
    }

    // Button state transition
    if (lastButtonState != currentButtonState) {
        halfPeriod = 0; // Set period to 0 to force light change

        // Turn off both lights
        digitalWrite(GREEN_LED, LOW);
        digitalWrite(RED_LED, LOW);
    }

    // Toggle pin if timer has passed
    if (currentTime >= startTime + halfPeriod) {
        startTime = currentTime; // Record new state start time
        digitalWrite(light, !digitalRead(light)); // Toggle pin
    }

    lastButtonState = currentButtonState; // Store button state
}


/**
 * Sets the pin modes
 */
void setPinModes() {
    pinMode(13, OUTPUT);
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(BUTTON, INPUT);
}


/**
 * Flashes the pin 13 LED at 1 Hz for 3 seconds
 */
void flashTest() {
    for (int i = 1; i <= 3; i++) {
        delay(500);
        digitalWrite(13, HIGH);
        delay(500);
        digitalWrite(13, LOW);
    }
}
