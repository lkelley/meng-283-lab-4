class Button
{
    private:
        short _pin;
        unsigned int _debounceTime;
        unsigned int _lastDebounce;
        int _buttonState;
        int _lastButtonState;
    
    public:
        Button();
        void setPin(int);
        int getState();
};